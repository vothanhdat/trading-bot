import { DepthChart } from "./service/depth"
import { TradeOrder } from "./service/tradeorder"
import events from 'events';
import fetch from 'node-fetch'


// new DepthChart("ETH_USDT")
class PeakManager {
  ar: number[] = []
  last: number
  addNewData(data) {
    if (data != this.last || this.last === undefined) {
      this.last = data
      this.ar.push(data)
    }
    if (this.ar.length > 10)
      this.ar.shift()
  }
  get length() {
    return this.ar.length
  }
  get min() {
    return Math.min(...this.ar)
  }
  get max() {
    return Math.max(...this.ar)
  }
  get delta() {
    return this.max - this.min
  }
  get lowPeak() {
    return this.min * 0.75 + this.max * 0.25
  }
  get highPeak() {
    return this.min * 0.25 + this.max * 0.75
  }
  get lastPeak(): [number, number] {
    var lowPeak = this.ar[this.ar.length - 1], highPeak = this.ar[this.ar.length - 1]
    for (var i = this.ar.length - 1; i >= 0; i--) {
      var current = this.ar[i]
      var precurrent = this.ar[i - 1]
      if (current > highPeak)
        highPeak = current
      if (current < lowPeak)
      lowPeak = current
      if (highPeak - lowPeak > 20 && (precurrent > lowPeak && precurrent < lowPeak))
        return [lowPeak, highPeak]
    }
    return [lowPeak, highPeak]
  }
}

class TradingManager extends events.EventEmitter {

  depth: DepthChart
  trade: TradeOrder

  constructor() {
    super()
    this.depth = new DepthChart("BTC_USDT")
    this.trade = new TradeOrder("BTC_USDT")
    var mid = 40
    var d = 5
    var price
    var pregap;
    var gapPeak = new PeakManager()
    var pricePeak = new PeakManager()

    this.depth.on(
      "priceGap",
      async (gap, percent) => {
        if (gap != pregap || price != this.depth.currentPrice) {


          price != this.depth.currentPrice && pricePeak.addNewData((this.depth.currentPrice * 10 | 0) / 10);
          price = price || this.depth.currentPrice;

          if ((gap | 0) != (pregap | 0) && gap >= 0 && gap <= 100)
            gapPeak.addNewData(gap | 0)


          let shouldUseGapPeak = false;//gapPeak.length > 10 && gapPeak.delta > 20
          let shouldUsePricePeak = pricePeak.length > 10 && (pricePeak.delta / pricePeak.min) > 0.0002
          let lowGapPeak = shouldUseGapPeak ? gapPeak.lowPeak : (mid - d)
          let highGapPeak = shouldUseGapPeak ? gapPeak.highPeak : (mid + d)

          console.info("                       ")
          console.info("GAP %d %d%\t PRICE %d", gap | 0, percent.toFixed(2), this.depth.currentPrice)
          console.info("GAP   --- %s --- %s", gapPeak.ar.join(' '), gapPeak.lastPeak.join(' '))
          console.info("PRICE --- %s --- %s", pricePeak.ar.join(' '), pricePeak.lastPeak.join(' '))
          // console.info({ ...gapPeak, lowPeak: gapPeak.lowPeak, highPeak: gapPeak.highPeak })
          // console.info({ ...pricePeak, lowPeak: pricePeak.lowPeak, highPeak: pricePeak.highPeak, ratio: (pricePeak.delta / pricePeak.min) })

          if (gap < lowGapPeak || (shouldUsePricePeak && price < pricePeak.lowPeak)) {
            for (var i = 0; i < 1; i++)
              this.trade.buyAll()

            await new Promise(r => setTimeout(r, 3000))
            await this.trade.fetchBalances();
            // console.log("current %s", this.trade.estimateBalance)

          }
          else if (gap > highGapPeak || (shouldUsePricePeak && price > pricePeak.highPeak)) {
            for (var i = 0; i < 1; i++)
              this.trade.sellAll()
            await new Promise(r => setTimeout(r, 3000))
            await this.trade.fetchBalances();
            // console.log("current %s", this.trade.estimateBalance)
          }

          price = this.depth.currentPrice;
          pregap = gap;

        }


      }
    )
    this.depth.on(
      "currentPrice",
      currentPrice => {
        this.trade.currentPrice = currentPrice
      }
    )


    // this.startTopTracker()
    // this.startControlPriceBot()

  }

  getTopProfit() {
    return fetch(
      'https://pt.kryptono.exchange/api/v1/ss/rank?competitionCode=JUMPSTART&limit=10',
      {
        headers: {
          "x-requested-with": "XMLHttpRequest",
        }
      }
    )
      .then(e => e.json())
      .then(e => e.data.find(f => f.accountId == "a2815e4c-b59c-4971-93f8-a48feaa1acaf").profit)
  }

  lastTop = undefined
  pauseTrading = false
  startTopTracker() {
    setInterval(async () => {
      if (this.pauseTrading)
        return;

      this.getTopProfit()
        .then(current => {
          if (this.lastTop && current > this.lastTop) {
            this.pauseTrading = true
            console.log("PAUSE TRADING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

            setTimeout(() => {
              this.getTopProfit().then(e => {
                this.lastTop = e
                this.pauseTrading = false
                console.log("RESUME TRADING >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
              })
            }, 10 * 60 * 1000 + 10 * 60 * 1000 * Math.random())
          }
          this.lastTop = current
        })
    }, 5000)

  }

  startControlPriceBot() {
    setInterval(async () => {
      if (this.pauseTrading)
        return;
      if (this.depth.gap < 0.4)
        return;

      this.trade.controlPrice(this.depth.startPrice)

      await new Promise(r => setTimeout(r, 5000))

      this.trade.controlPrice(this.depth.endPrice)
    }, 10000)
  }


}

var trading = new TradingManager()

