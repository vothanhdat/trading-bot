## Requirement
node > 10

## Install
```bash
npm install -g nodemon
npm install
```

## Run
```bash
npm start
```