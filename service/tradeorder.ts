import events from 'events';
import fetch from 'node-fetch'


//Config key here
const ptCookie = ""
const exCookie = ""

export class TradeOrder extends events.EventEmitter {

  currentPrice: number = 0


  headers = {
    "authentication": "",
    "cookie": ptCookie,
    "referer": `https://pt.kryptono.exchange/trade?c=JUMPSTART&s=${this.pair}`,
  }

  balances: { [k: string]: { totalBalances: number, availableBalances: number, inOrder: number, coinName: string } } = {}

  _timeout: any

  constructor(public pair: string) {
    super()
    this.renewToken()
      .then(() => this.fetchBalances())
    setInterval(this.renewToken.bind(this), 1000 * 60 * 5)
  }


  get toSellCoin() {
    return this.pair.split("_")[0]
  }


  get toBuyCoin() {
    return this.pair.split("_")[1]
  }

  get toSellBalance() {
    try {
      return this.balances[this.toSellCoin]
    } catch (error) {
      return null
    }
  }

  get toBuyBalance() {
    try {
      return this.balances[this.toBuyCoin]
    } catch (error) {
      return null
    }
  }

  renewToken() {
    return fetch(
      "https://kryptono.exchange/k/accounts/tokens/get",
      {
        "headers": {
          "cookie": ptCookie,
          "x-requested-with": "XMLHttpRequest",
        },
        "body": null,
        "method": "GET",
      }
    )
      .then(e => e.json())
      .then(e => {
        if (e.token) {
          this.headers.authentication = e.token
          console.log("renew Token successfully")
        }
      })
      .catch(e => {
        console.error(e)
        throw e;
      })
  }

  fetchBalances() {
    return fetch(
      "https://pt.kryptono.exchange/api/v1/balance/list?competitionCode=JUMPSTART",
      {
        "headers": this.headers,
        "body": null,
        "method": "GET",
      })
      .then(e => e.json())
      .then(e => {
        // console.log(e.data)
        this.balances = {}

        for (var data of e.data)
          this.balances[data.coinName] = data
        return this.balances
      })
      .catch(e => {
        console.log("Try again after 10s")
        clearTimeout(this._timeout)
        this._timeout = setTimeout(this.fetchBalances.bind(this), 10000)
        throw e
      })
  }

  cancelOrder(id) {
    console.log("Cancel id %s", id)
    return fetch(
      "https://pt.kryptono.exchange/api/v1/trading/order/cancel",
      {
        "headers": {
          ...this.headers,
          "content-type": "application/json;charset=UTF-8",
          "accept": "application/json, text/plain, */*"
        },
        "body": JSON.stringify({ id }),
        "method": "POST",
      }).then(e => e.json()).then(e => console.log(e))
  }

  placeOrder({ side, amount, price = this.currentPrice }) {
    return fetch(
      "https://pt.kryptono.exchange/api/v1/trading/order",
      {
        "headers": {
          ...this.headers,
          "content-type": "application/json;charset=UTF-8",
          "accept": "application/json, text/plain, */*"
        },
        "body": JSON.stringify({
          "pair": `${this.pair}`,
          "side": side,
          "amount": amount,
          "type": "limit",
          "trigger": "",
          "competitionCode": "JUMPSTART",
          "price": price
        }),
        "method": "POST",
      })
      .then(e => e.json())
      .then((e) => {
        try {
          if (e.data.id)
            setTimeout(() => this.cancelOrder(e.data.id), 2000)
          for (var data of e.data.coinBalances)
            this.balances[data.coinName] = data
        } catch (error) { }
      })
  }

  async sellAll(price = this.currentPrice) {
    if (this.toSellBalance) {
      var availableBalances = this.toSellBalance.availableBalances
      if (availableBalances > 0.01) {
        console.log("sellAll                >>>>>>>>>>>>>> ", availableBalances)
        await this.placeOrder({ side: "sell", amount: availableBalances, price })
        await new Promise(r => setTimeout(r, 2300))
        // await this.fetchBalances()
      }
    }
  }

  async buyAll(price = this.currentPrice) {
    if (this.toBuyBalance) {
      var availableBalances = this.toBuyBalance.availableBalances
      var toBuyAmount = this.currentPrice && availableBalances / this.currentPrice || 0
      if (this.currentPrice > 0 && toBuyAmount > 0.01) {
        console.log("buyAll <<<<<<<<<<<<<<            ", toBuyAmount)
        await this.placeOrder({ side: "buy", amount: 0.99999 * toBuyAmount, price })
        await new Promise(r => setTimeout(r, 2300))
        // await this.fetchBalances()
      }
    }
  }

  get dataBalance() {
    return Object.values(this.balances).map(e => ([e.coinName, e.availableBalances]))
  }

  get estimateBalance() {
    try {
      return parseFloat((this.balances.ETH || {} as any).availableBalances * this.currentPrice as any || 0) + parseFloat((this.toBuyBalance || {} as any).availableBalances as any || 0)
    } catch (error) {
      return 0
    }
  }


  controlPrice(price) {
    return Promise.all([
      fetch(
        "https://kryptono.exchange/k/order/add_order",
        {
          "headers": {
            ...this.headers,
            "cookie": exCookie,
            "content-type": "application/json;charset=UTF-8",
            "accept": "application/json, text/plain, */*",
            "x-requested-with": "XMLHttpRequest",
            "referer": "https://kryptono.exchange/k/accounts/trade?s=ETH_USDT",
            "origin": "https://kryptono.exchange",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
          },
          "body": JSON.stringify({
            order_price: price.toFixed(3),
            order_side: "buy",
            order_size: (10.1 / price).toFixed(5),
            order_symbol: this.pair,
            stop_price: "",
            type: "limit",
          }),
          "method": "POST",
        })
        .then(e => e.json()),
      fetch(
        "https://kryptono.exchange/k/order/add_order",
        {
          "headers": {
            ...this.headers,
            "cookie": exCookie,
            "content-type": "application/json;charset=UTF-8",
            "accept": "application/json, text/plain, */*",
            "x-requested-with": "XMLHttpRequest",
            "referer": "https://kryptono.exchange/k/accounts/trade?s=ETH_USDT",
            "origin": "https://kryptono.exchange",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
          },
          "body": JSON.stringify({
            order_price: price.toFixed(3),
            order_side: "sell",
            order_size: (10.1 / price).toFixed(5),
            order_symbol: this.pair,
            stop_price: "",
            type: "limit",
          }),
          "method": "POST",
        })
        .then(e => e.json())
    ]).catch(e => console.error(e))
      .then(e => console.log(e))
  }

}