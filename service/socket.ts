import events from 'events';
import WebSocket from 'ws';



export class WebSocketManager extends events.EventEmitter {

  lastTicker: number
  _intervalChecking: NodeJS.Timeout
  socket: WebSocket

  constructor(public wsPath: string) {
    super();
    this.lastTicker = 0;
    this.initSocket(wsPath)
    this._intervalChecking = setInterval(() => this.intervalChecking(), 5000);
  }

  initSocket(ws) {
    try {
      this.close()
    } catch (error) { }

    this.lastTicker = Date.now();
    this.socket = new WebSocket(this.wsPath)

    this.socket.on('message', (data) => {
      this.lastTicker = Date.now();
      this.emit('message', data)
    });

    this.socket.on(
      'error',
      (error) => {
        this.emit('error', error)
      }
    );
  }

  intervalChecking() {
    if (Date.now() - this.lastTicker > 20000) {
      this.initSocket(this.wsPath);
    }
  }

  close() {
    this.socket.removeAllListeners();
    this.socket.close();
  }

  remove() {
    clearInterval(this._intervalChecking);
  }
}




