import { WebSocketManager } from './socket'
import fetch from 'node-fetch'
import events from 'events'

export class DepthChart extends events.EventEmitter {

  depthSocket: WebSocketManager
  priceSocket: WebSocketManager

  depthChart: { asks: [number, number][], bids: [number, number][], } = { asks: [], bids: [] }
  currentPrice: number = 0

  constructor(public pair: string) {
    super();
    fetch(`https://engine2.kryptono.exchange/api/v1/dp?symbol=${pair}`)
      .then(e => e.json())
      .then(({ asks, bids }) => {
        this.depthChart = {
          asks: asks.map(([e, f]) => ([parseFloat(e), parseFloat(f)])),
          bids: bids.map(([e, f]) => ([parseFloat(e), parseFloat(f)])),
        }
      })

    this.depthSocket = new WebSocketManager(`wss://engine2.kryptono.exchange/ws/v1/dp/${pair}`)
    this.depthSocket.on("message", data => {
      try {
        const { a: asks, b: bids } = JSON.parse(data)
        this.onSocketUpdate({
          asks: asks.map(([e, f]) => ([parseFloat(e), parseFloat(f)])),
          bids: bids.map(([e, f]) => ([parseFloat(e), parseFloat(f)])),
        })
      } catch (error) {
      }
    })

    this.priceSocket = new WebSocketManager(`wss://engine2.kryptono.exchange/ws/v1/cs/${pair}@1m`)
    this.priceSocket.on("message", data => {
      try {
        const { k: { c } } = JSON.parse(data)
        this.currentPrice = parseFloat(c)
        this.emit("currentPrice", this.currentPrice)
        setTimeout(() => this.onLog(), 40)
      } catch (error) {
        // console.log(error)
      }
    })
  }

  onSocketUpdate({ asks, bids }: { asks: [number, number][], bids: [number, number][], }) {


    var asksIdx: { [k: number]: [number, number] } = this.depthChart.asks.reduce((o, e) => (o[e[0]] = e, o), {})
    var bidsIdx: { [k: number]: [number, number] } = this.depthChart.bids.reduce((o, e) => (o[e[0]] = e, o), {})

    for (var ask of asks)
      if (asksIdx[ask[0]])
        asksIdx[ask[0]][1] = ask[1]
      else
        this.depthChart.asks.push(ask);

    for (var bid of bids)
      if (bidsIdx[bid[0]])
        bidsIdx[bid[0]][1] = bid[1]
      else
        this.depthChart.bids.push(bid);

    this.depthChart.asks.sort((e, f) => e[0] - f[0])
    this.depthChart.bids.sort((e, f) => f[0] - e[0])

    this.depthChart.asks = this.depthChart.asks.filter(e => e[1] > 0)
    this.depthChart.bids = this.depthChart.bids.filter(e => e[1] > 0)


    // this.onLog()
  }

  get gap() {
    var min = this.depthChart.bids[0][0]
    var max = this.depthChart.asks[0][0]
    return (max - min) / (max + min) * 2 * 100
  }

  get priceGap() {
    var min = this.depthChart.bids[0][0]
    var max = this.depthChart.asks[0][0]
    return 100 * (this.currentPrice - min) / (max - min)
  }

  get startPrice() {
    return this.depthChart.bids[0][0] + 0.02
  }

  get endPrice() {
    return this.depthChart.asks[0][0] - 0.02
  }

  onLog() {
    // var min = this.depthChart.bids[0][0]
    // var max = this.depthChart.asks[0][0]
    // console.log("\n".repeat(20))
    // console.log("%d  %d  %f", min, max, this.currentPrice)
    // console.log("GAP ", this.gap, " %")
    // console.log("Price Gap", this.priceGap, " %")
    this.emit("priceGap", this.priceGap, this.gap)
  }
}