


export function timeoutTask(timeout, doTask) {
  var lastUpdate = 0;
  var updateTask = null;
  console.log('GENERATE')
  return async function updateData(...args) {
    if (updateTask)
      return await updateTask
    if (Date.now() - lastUpdate > timeout) {
      lastUpdate = Date.now();
      updateTask = updateTask || doTask(...args);
      console.log("updateTask", updateTask)
      await updateTask;
    }
    updateTask = null;
  }
}